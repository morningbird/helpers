<?php

namespace morningbird\helpers;

class Formatter extends \yii\i18n\Formatter {
    public function asCurrency($value, $currency = null, $options = [], $textOptions = [])
    {
        return number_format($value, 0, ",", ".");
    }
}
