<?php

namespace morningbird\helpers;

use Yii;

class Helper {
    
    static function getMonthList()
    {
        
        $arr = [];
        for($i=1;$i<=12;$i++)
        {
            $arr[$i] = \Yii::t('app', date('F', strtotime("2017-{$i}-1")));
        }
        return $arr;
    }
    
    static function sendMail($to, $subject, $params = [], $layout = 'blank')
    {
        if(is_string($params))
        {
            $params = ['content' => $params];
        }
//        \Yii::$app->mailer->compose('@app/mail/layouts/' . $layout, $params)
//                ->setFrom('no-reply@centralpowerbank.com')
//                ->setTo($to)
//                ->setSubject($subject)
//                ->send();
    }

    static function isNewProduct($id)
    {
        $key = 'new_product';
        $cacheResult = Yii::$app->cache->get($key);
        if($cacheResult===FALSE)
        {
            $temp = \app\models\Product::getLatestProduct(20, ['id']);
            $cacheResult = [];
            foreach($temp as $row)
            {
                $cacheResult[] = $row->id;
            }
            Yii::$app->cache->set($key, $cacheResult, 300);
        }
        if(in_array($id, $cacheResult))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static function sendNotifToAdmin($message, $link = null)
    {
        $notif = new Notification();
        $notif->message = $message;
        $notif->link = $link;
        $notif->save();
        get_async(yii\helpers\Url::to(['mail/sendnotif', 'id' => $notif->id], true));
    }

    static function get_async($url, array $params = array())
    {
        $post_string = '';
        if(count($params)>0)
        {
            foreach ($params as $key => &$val) {
                if (is_array($val)) $val = implode(',', $val);
                    $post_params[] = $key.'='.urlencode($val);  
            }
            $post_string = implode('&', $post_params);
        }

        $parts=parse_url($url);

        $fp = fsockopen($parts['host'],
            isset($parts['port'])?$parts['port']:80,
            $errno, $errstr, 30);

        $out = "GET ".$parts['path'] . "?" . $parts['query'] ." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;

        fwrite($fp, $out);
        fclose($fp);
    }

    static function getNumber($number)
    {
        return Yii::$app->formatter->asInteger($number);
    }

    static function getPrice($number)
    {
      return 'IDR ' . Yii::$app->formatter->asInteger($number);
    }

    static function getOngkir($number)
    {
        if($number>0)
        {
            return Yii::$app->formatter->asCurrency($number);
        }
        else if($number==\app\components\Rajaongkir::INVALID)
        {
            return 'Terjadi kesalahan saat meload data';
        }
        else if($number==\app\components\Rajaongkir::NOT_AVAILABLE)
        {
            return 'Pengiriman ke area ini belum terjangkau';
        }
    }


    static function getPriceProduct($number)
    {
        //jika kosong, anggap call for price
        if(empty($number) || $number==0)
        {
            return 'Belum Tersedia';
        }
        //lainnya
        return 'IDR ' . Yii::$app->formatter->asInteger($number);
    }

    static function getWeight($number)
    {
        $number = self::getNumber($number);
        if($number==0)
        {
            return '-';
        }
        else
        {
            return $number . ' gr';
        }
    }

    static function getMyDate($myDate = 'now', $format = 'd-m-Y')
    {
        if(empty($myDate))
        {
            return null;
        }
        else if($myDate==='now')
        {
            $myDate = date('Y-m-d');
        }
        return Yii::$app->formatter->asDate($myDate, 'php:' . $format);
    }

    /**
     * Membersihkan nama file
     * Referensi: http://stackoverflow.com/questions/2021624/string-sanitizer-for-filename
     * @param string $oldName
     * @return string
     */
    static function sanitizeFileName($oldName)
    {
        $newName = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $oldName);
        $newName = mb_ereg_replace("([\.]{2,})", '', $newName);
        $newName = str_replace(' ', '-', $newName);
        return $newName;
    }
}
